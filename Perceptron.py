import numpy as np
import pandas as pd

class Perceptron():

    """Perceptron classifier

    Parameters

    eta: float
        learning rate (between 0.0 and 1.0)
    n_iter: int
        number of passes over training dataset
    random_seed: int
        Random number generator seed for random weight initialization

    Attributes

    w_: 1d-array
        Weights after fitting
    errors_: list
        Number of misclassifications (updates) in each epoch.
    """

    def __init__(self, eta=0.01, n_iter=50, random_seed=1):
        self.eta = eta
        self.n_iter = n_iter
        self.random_seed = random_seed

    def fit(self, X, y):
        """
        Fits training data

        Parameters

        X: {array-like}, shape = [n_samples, n_features]
            Training vectors, where n_samples is the number of
            samples and n_features is the number of features
        y: array-like, shape = [n_samples]
            Label values
        """

        rgen = np.random.RandomState(self.random_seed)
        self.w_ = rgen.normal(loc=0.0,scale=0.01, size=1 + X.shape[1])

        self.errors_ = []

        for i in range(self.n_iter):
            errors = 0
            for xi, target in zip(X, y):
                update = self.eta * (target - self.predict(xi))
                self.w_[1:] += update * xi
                self.w_[0] += update
                errors += int(update != 0.0)
            self.errors_.append(errors)
        return self

    def net_input(self, X):
        return np.dot(X, self.w_[1:]) + self.w_[0]

    def predict(self, X):
        return np.where(self.net_input(X) >= 0.0, 1, -1)